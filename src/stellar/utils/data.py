"""
This module contains different functions to load the data into different
formats.
"""
import os
import numpy as np
import pandas as pd


def load_data_as_numpy(test=False, data_dir='../data'):
    """
    Loads the dataset.

    Parameters
    ----------
    test : boolean
        If true, then the test dataset is loaded. The train dataset is loaded
        otherwise.
    data_dir : string
        The path to the data directory.

    Returns
    -------
    A tuple of two numpy arrays. The first one contains the independant
    variables and the second, the dependant variable as string.
    """
    X = np.load(
        os.path.join(data_dir, ('X_test.npy' if test else 'X_train.npy'))
    )
    y = np.load(
        os.path.join(data_dir, ('y_test.npy' if test else 'y_train.npy'))
    )
    return X, y


def load_data_as_pandas(test=False, data_dir='../data',
                        x_labels=['alpha', 'delta', 'u', 'g', 'r', 'i', 'z',
                                  'redshift']):
    """
    Loads the dataset.

    Parameters
    ----------
    test : boolean
        If true, then the test dataset is loaded. The train dataset is loaded
        otherwise.
    data_dir : string
        The path to the data directory.
    x_labels : list of string
        The labels of the independant variables.

    Returns
    -------
    A Pandas dataframe containing the independant and the dependant variables.
    """
    X, y = load_data_as_numpy(test, data_dir)
    df = pd.DataFrame(X, columns=x_labels)
    df['class'] = y
    return df
