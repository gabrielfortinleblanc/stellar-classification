#!/bin/bash
# This script executes multiple applications to analysis the code. It is
# supposed that this script is executed inside the
# project's Python environment.
# Setup environment variables
PYTHONDONTWRITEBYTECODE=1

echo 'Print stylistic errors with the package flake8'
echo '----------------------------------------------'

# Print the stylistic errors
flake8 src tests

# Execute the tests and print a coverage report. To specify the behavior, see
# the files `.coveragerc` and `pytest.ini`.
coverage run --source=src -m pytest
coverage report -m
